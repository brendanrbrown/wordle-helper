My friend wanted me to play Wordle. I didn't want to spend time playing Wordle, but I did want to spend time with my friend.

Word lists from https://github.com/dwyl/english-words/, many of which are not Wordle words, and https://ftp.gnu.org/gnu/aspell/dict/en/.

The application ranks possible guesses by the sum of normalized frequencies for each letter in the guess, across all letters in the working list of possible words. A letter's score in the guess word is counted only once, but the frequencies computed do consider duplicates. Scoring can be improved, for example by accounting for position or other
patterns. 

For a more sophisticated approach using entropy ranks, see [this video by
3Blue1Brown](https://www.3blue1brown.com/lessons/wordle).

## Performance
**Games:** 34
**Average score:** 3.71
