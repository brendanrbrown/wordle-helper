//! Defines the WordleMap struct, along with functions to
//! score words and read different word lists into
//! WordleMap.
const std = @import("std");
const SMap = std.StringArrayHashMap;

/// Wrapper for `std.StringArrayHashMap` with some
/// methods specialized to wordle_helper.
pub const WordleMap = struct {
    data: SMap(u64),

    const Self = @This();
    const Iterator = SMap(u64).Iterator;

    pub fn init(allocator: std.mem.Allocator) Self {
        return .{ .data = SMap(u64).init(allocator) };
    }

    pub fn keys(self: Self) [][]const u8 {
        return self.data.keys();
    }

    pub fn values(self: Self) []u64 {
        return self.data.values();
    }

    pub fn count(self: Self) usize {
        return self.data.count();
    }

    pub fn get(self: *Self, key: []const u8) ?u64 {
        return self.data.get(key);
    }

    pub fn put(self: *Self, key: []const u8, value: u64) !void {
        try self.data.put(key, value);
    }

    // Though self is *const, you can mutate through the iterator. See
    // https://ziglang.org/documentation/master/std/src/array_hash_map.zig.html#L615
    pub fn iterator(self: *const Self) Iterator {
        return self.data.iterator();
    }

    pub fn sort_values(self: *Self) void {
        self.data.sort(WordleCtx{ .scores = self.values() });
    }

    /// Set the score (value) for each word (key) in the
    /// `WordleMap` using a `freq_map` of weights for
    /// each letter. Each word is scored using `score_word`.
    pub fn setScores(self: *const Self, freq_map: std.AutoHashMap(u8, u64)) !void {
        var it = self.iterator();
        var score: u64 = undefined;

        while (it.next()) |e| {
            score = try score_word(e.key_ptr.*, freq_map);
            e.value_ptr.* = score;
        }
    }
};

// Taken from test sort in
// https://ziglang.org/documentation/master/std/src/array_hash_map.zig.html#L411

/// Context for sorting a `WordleMap` by score value, in descending order.
pub const WordleCtx = struct {
    scores: []u64,

    // descending order, by value (score). then use values slice.
    pub fn lessThan(ctx: @This(), a_index: usize, b_index: usize) bool {
        return ctx.scores[a_index] > ctx.scores[b_index];
    }
};

/// For the given `word` and provided `freq` map of
/// letter weights, compute the 'score' of a word by
/// summing the weights for each *unique* letter in the
/// word, plus one.
pub fn score_word(word: []const u8, freq: std.AutoHashMap(u8, u64)) !u64 {
    var buffer: [1000]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&buffer);
    var allocator = fba.allocator();

    var cm = std.AutoHashMap(u8, void).init(allocator);
    defer cm.deinit();

    var score: u64 = 0;

    for (word) |c| {
        // if the key doesn't already exist, put one there and return null.
        // in that case, + (1 + freq_score). if fetching from freq fails, set
        // the freq_score to zero.  note this adds score only once for each
        // unique letter in the word.
        var res = try cm.fetchPut(c, {});
        if (res == null) {
            const freq_score = freq.get(c) orelse 0;
            score += 1 + freq_score;
        }
    }
    return score;
}

// READ DATA

/// Read a word list in text format from `path`, with a
/// single line per word. Returns a `WordleMap` with
/// `undefined` values and keys only words of length
/// `wlen` from the list.
pub fn read_wordlist_lines(path: []const u8, wlen: usize, max_size: usize, allocator: std.mem.Allocator) !WordleMap {
    const file = try std.fs.cwd().openFile(path, .{ .mode = .read_only });
    defer file.close();

    // TODO in latest zig this is renamed readToEndAlloc
    const data = try file.reader().readAllAlloc(allocator, max_size);

    var wm = WordleMap.init(allocator);

    var lines = std.mem.split(u8, data, "\n");

    while (lines.next()) |l| {
        if (include_word_in_wordlist(l, wlen)) try wm.put(l, undefined);
    }

    return wm;
}

/// Return true if the line should be included in the initial list.
fn include_word_in_wordlist(l: []const u8, wlen: usize) bool {
    var b = (l.len == wlen);

    for (l) |c| {
        b = b and std.ascii.isAlphabetic(c) and std.ascii.isLower(c);
    }

    return b;
}

/// TODO descripton.
pub fn read_json_wordle(path: []const u8, wlen: usize, max_size: usize, allocator: std.mem.Allocator) !WordleMap {
    // TODO think through how allocation is handled here, at the various steps and in the return.
    const file = try std.fs.cwd().openFile(path, .{ .mode = .read_only });
    defer file.close();

    const data = try file.reader().readAllAlloc(allocator, max_size);

    var parser = std.json.Parser.init(allocator, true);

    var wl = try parser.parse(data);

    var wm = WordleMap.init(allocator);

    for (wl.root.Object.keys()) |k| {
        if (k.len == wlen) {
            try wm.put(k, undefined);
        }
    }

    return wm;
}
