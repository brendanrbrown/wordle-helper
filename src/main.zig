//! TODO
const std = @import("std");
const wordlist = @import("wordlist");

const eql = std.mem.eql;
const heap = std.heap;
const AAllocator = heap.ArenaAllocator;

const WordleMap = wordlist.WordleMap;
const read_json_wordle = wordlist.read_json_wordle;
const score_word = wordlist.score_word;
const read_wordlist_lines = wordlist.read_wordlist_lines;

// Wordle structs

// Keep this separate rather than inferred for ease of use in parsing user
// input.
const IndexHintEnum = enum { incl, excl, never };

const IndexHint = union(IndexHintEnum) {
    // Green wordle box: letter must be at this index.
    incl: usize,
    // Yellow wordle box: letter included but not at this index.
    excl: usize,
    // TODO value needed?
    // Grey wordle box: letter never included.
    never: void,
};

// letter found. if include, the letter must be at that index. if exclude
// cannot be at that index. if never, cannot be anywhere.
const WordleHint = struct {
    letter: u8,
    index: IndexHint,
};

const HintSlice = []const WordleHint;

const WordChoiceOpts = enum {
    cont,
    more,
    invalid_choice,
};

// 'wordle' word length and max number of guesses
const wordle_len: usize = 5;
const max_guesses: usize = 6;

pub fn main() !void {

    // SETUP:
    // * read the data
    // * compute the scores
    // * return a hashmap of each word with its score

    // TODO why the {}?
    var gpa = heap.GeneralPurposeAllocator(.{}){};
    defer if (gpa.deinit()) @panic("GPA LEAK");

    // TODO can i just pass the gpa allocator around rather than using the arena?
    // do i need to / want to do anything other than deinit the arena?
    var arena = AAllocator.init(gpa.allocator());
    defer arena.deinit();

    var allocator = arena.allocator();

    //const path = "./words_dictionary.json";
    const max_size = 100_000_000;
    // larger means more sensitive to letter frequency than uniqueness.
    const scale_score = 80;

    // Create the words hashmap and sort it
    // TODO create cli args to choose the word list
    //var wl = try read_json_wordle("words_dictionary.json", wordle_len, max_size, allocator);
    var wl = try read_wordlist_lines("en-common.txt", wordle_len, max_size, allocator);

    // Frequency map, score and sort
    // TODO use allocator instead of arena.allocator?
    var freq_map = try letter_freq(wl.keys(), arena.allocator(), scale_score);
    try wl.setScores(freq_map);
    wl.sort_values();

    // CLI
    // * prompt user to enter a guess in the wordle app (not here)
    // * display top 5 (by score) of the remaining valid words to guess, with an option to see more.
    // * prompt user to enter wordle's 'hints' one at a time, consisting of a
    // letter known to be in the final word and an optional position for where
    // that letter is known to be.
    // * break once all hints entered, repeat above two steps
    // * stop after 5 rounds (parameterized as max_guesses)

    // Setup
    // stdin buffer
    // TODO dealing with buffer overflow in stdin?
    var buffer: [10]u8 = undefined;

    // Loop counters/conditions
    var guesses: u8 = 0;
    var word_count: usize = undefined;
    var choice_idx_start: usize = undefined;
    var choice_idx_end: usize = undefined;
    var choice_i: usize = undefined;
    var guess_len: usize = undefined;

    // Choice buffers
    // TODO can't fill this from []u8?
    //var guess_buffer: [wordle_len]u8 = undefined;
    var guess_buffer: [wordle_len]u8 = undefined;
    var word_buffer: [][]const u8 = undefined;
    var score_buffer: []const u64 = undefined;
    var current_hint: WordleHint = undefined;
    var hint_buffer: [wordle_len]WordleHint = undefined;
    var hint_enum: ?IndexHintEnum = undefined;
    var choice_enum: WordChoiceOpts = undefined;

    // Messages
    const msg_word_choices = "\nBest choices (cont for continue, enter/more for more):\n";
    //const msg_hint_pos = "Position (first index is 0): ";
    const msg_hint_incl = "\nletter: {u}, position: {d}\nEnter incl (green), excl (yellow), never (default): ";
    const msg_final = "\nNo more guesses!\n";

    // Format strings
    const fmt_choices = "{s}: {d} (score)\n";

    // Stdin
    const stdin = std.io.getStdIn();
    defer stdin.close();
    var resp = stdin.reader();
    // TODO these can be cleaned up with better understanding of slice copies for input
    var line: []u8 = undefined;
    //TODO remove
    //var line_int: []u8 = undefined;
    var line_idx_hint: []u8 = undefined;

    guess_loop: while (guesses < max_guesses) {

        // CHOICES LIST

        // Init
        word_count = wl.count();
        choice_idx_start = 0;
        choice_idx_end = std.math.min(choice_idx_start + 10, word_count);

        std.debug.print("{d} possible guesses.\n", .{word_count});

        word_buffer = wl.keys();
        score_buffer = wl.values();

        // Start choice loop
        std.debug.print(msg_word_choices, .{});

        // Control flow handled by 'else' arm below
        choice_loop: while (choice_idx_end <= word_count) {
            for (word_buffer[choice_idx_start..choice_idx_end]) |w, i| {
                // i is relative to input slice not original
                choice_i = choice_idx_start + i;
                std.debug.print(fmt_choices, .{ w, score_buffer[choice_i] });
            }

            // TODO improve flow logic, remove repetition, etc..
            if (choice_idx_end == word_count) {
                std.debug.print("Choices exhausted. Restart (enter) or continue (cont)?.\n", .{});

                line = try resp.readUntilDelimiter(&buffer, '\n');

                choice_enum = std.meta.stringToEnum(WordChoiceOpts, line) orelse WordChoiceOpts.invalid_choice;

                switch (choice_enum) {
                    .cont => break,
                    else => {
                        // restart
                        choice_idx_start = 0;
                        choice_idx_end = std.math.min(choice_idx_start + 10, word_count);

                        continue :choice_loop;
                    },
                }
            }

            line = try resp.readUntilDelimiter(&buffer, '\n');
            choice_enum = std.meta.stringToEnum(WordChoiceOpts, line) orelse default: {
                if (line.len == 0) {
                    break :default WordChoiceOpts.more;
                } else {
                    break :default WordChoiceOpts.invalid_choice;
                }
            };

            switch (choice_enum) {
                .cont => break,
                .more => {
                    // reloop
                    choice_idx_start = choice_idx_end;
                    choice_idx_end = std.math.min(choice_idx_start + 10, word_count);

                    continue :choice_loop;
                },
                .invalid_choice => {
                    std.debug.print("Invalid input.\n", .{});
                    continue :choice_loop;
                },
            }
        }

        // HINTS
        // Enter guess made
        guess_len = 0;

        enter_guess_loop: while (guess_len != wordle_len) {
            std.debug.print("\nEnter your guess:\n", .{});
            line = try resp.readUntilDelimiter(&buffer, '\n');

            // TODO is loop cond evaluated immediately?
            guess_len = line.len;

            if (guess_len != wordle_len) {
                std.debug.print("Guesses must have {d} letters.", .{wordle_len});
                continue :enter_guess_loop;
            } else {
                // TODO remove. note this led to odd issues because you are
                // reading into line many times.
                //guess_buffer = line;
                std.mem.copy(u8, &guess_buffer, line);
                break;
            }
        }

        // TODO are these values copied? std.mem.copy?
        // TODO input validity checks, better handling of optional, int
        // errs etc, single-char condition, etc.
        std.debug.print("\nFor each letter enter one of the inclusion criteria (wordle color in parentheses).", .{});

        for (guess_buffer) |c, i| {
            // TODO use enum for commands
            // https://github.com/sayden/zig-snippets/blob/main/stdin_while.zig
            std.debug.print(msg_hint_incl, .{ c, i });
            line_idx_hint = try resp.readUntilDelimiter(&buffer, '\n');

            // Parse
            hint_enum = std.meta.stringToEnum(IndexHintEnum, line_idx_hint);

            if (hint_enum) |e| {
                switch (e) {
                    .incl => current_hint = WordleHint{ .letter = c, .index = .{ .incl = i } },
                    .excl => current_hint = WordleHint{ .letter = c, .index = .{ .excl = i } },
                    .never => current_hint = WordleHint{ .letter = c, .index = .{ .never = {} } },
                }
            } else {
                if (line_idx_hint.len != 0) {
                    std.debug.print("Got invalid input {s}\nDefaulting to never", .{line_idx_hint});
                }

                current_hint = WordleHint{ .letter = c, .index = .{ .never = {} } };
            }

            hint_buffer[i] = current_hint;
        }

        // TODO wrap the above in a while(true) loop, and prompt for a
        // confirmation here, redoing the loop if no.
        std.debug.print("\nHints entered:\n", .{});
        for (&hint_buffer) |wh| {
            std.debug.print("Entered letter: {u}, index: {any}\n", wh);
        }

        // THIN GUESS LIST
        std.debug.print("Recomputing best guesses...\n", .{});

        wl = try thin_words(&wl, &hint_buffer, arena.allocator());
        freq_map = try letter_freq(wl.keys(), arena.allocator(), scale_score);
        try wl.setScores(freq_map);
        wl.sort_values();

        std.debug.print("Done.\n", .{});

        // TODO is this needed?
        word_buffer = undefined;
        score_buffer = undefined;

        //TODO just for the name
        continue :guess_loop;
    }

    // TODO holdover from zig init
    // stdout is for the actual output of your application, for example if you
    // are implementing gzip, then only the compressed bytes should be sent to
    // stdout, not any debugging messages.
    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    const stdout = bw.writer();

    try stdout.print(msg_final, .{});

    // try not allowed in defer
    try bw.flush();
}

/// Calculate the frequency of each letter among a list of words. +1 for each
/// time a letter appears in a word. That is then scaled by the total number of
/// characters across all words, times the scale argument.  Caller handles
/// deinit.
fn letter_freq(wl: [][]const u8, allocator: std.mem.Allocator, scale: u64) !std.AutoHashMap(u8, u64) {
    // Total characters across all words to be computed. Used in score.
    var total: u64 = 0;

    var out = std.AutoHashMap(u8, u64).init(allocator);

    for (wl) |word| {
        for (word) |c| {
            // count chars
            total += 1;

            var elem = out.getEntry(c);

            // if the character doesn't exist, add it with count one. if it does, increment.
            if (elem) |e| {
                e.value_ptr.* += 1;
            } else {
                try out.put(c, 1);
            }
        }
    }

    // Scale all scores by the total, times scale parameter, with a min of 0.
    var it = out.iterator();
    // placeholder for interim calc
    var score: u64 = undefined;

    while (it.next()) |elem| {
        score = (scale * elem.value_ptr.*) / total;
        elem.value_ptr.* = score;
    }

    return out;
}

// Check whether a character is in a word, with at least one instance at the
// specified index.
fn check_incl(word: []const u8, letter: u8, idx: usize) bool {
    var b = false;
    for (word) |c, i| {
        b = b or ((letter == c) and (idx == i));
    }

    return b;
}

// Check a character is in a word, with at least one instance not at the
// forbidden index.
fn check_excl(word: []const u8, letter: u8, idx: usize) bool {
    var b = false;

    for (word) |c, i| {
        b = b or ((letter == c) and (idx != i));
    }

    return b;
}

// Check that a letter is not in a word.
fn check_never(word: []const u8, letter: u8) bool {
    if (std.mem.indexOfScalar(u8, word, letter)) |_| {
        return false;
    } else {
        return true;
    }
}

/// Check a word based on a wordle guess result.
/// This doesn't care about word length. Returns true if it should be kept.
fn check_word(word: []const u8, wres: HintSlice) !bool {
    // true means include the word
    // default is include, e.g. if there are no hints
    var b = true;

    // keep track of green and yellow letters found, for use in checking the
    // .never condition, esp for multiples.
    var letter_set: [wordle_len]u8 = undefined;

    // So that the word is not modified in place.
    std.mem.copy(u8, &letter_set, word);

    for (wres) |hint| {
        switch (hint.index) {
            // idx is what it must be for inclusion (green wordle box)
            .incl => |idx| {
                if (letter_set[idx] == hint.letter) {
                    b = b and true;
                    // if found, this occurrence of the letter should not be
                    // considered in .never or .excl, so it is replaced
                    // with the wordle-invalid char '~', which unlike
                    // `orderedRemove` retains the indexing of remaining
                    // letters.
                    // TODO this char should be set up top.
                    letter_set[idx] = '~';
                } else {
                    b = false;
                }
            },
            // idx is not the one, but letter must be included (yellow
            // wordle box)
            .excl => |idx| {
                if (std.mem.indexOfScalar(u8, &letter_set, hint.letter)) |i| {
                    // indexOfScalar returns the first match, so now
                    // need to check there isn't another in the
                    // forbidden spot.
                    if (letter_set[idx] != hint.letter) {
                        b = b and true;
                        // A letter can only be considered once for this
                        // condition, and if considered for .excl should not be
                        // for .never or another .excl.
                        letter_set[i] = '~';
                    } else {
                        b = false;
                    }
                } else {
                    b = false;
                }
            },
            // never computed separately (grey wordle box)
            .never => |_| {},
        }

        // false reached means the word should not be included.
        if (!b) break;
    }

    // TODO is there a way to organize this logic to avoid needing to do the
    // .never condition as a second pass?
    // Note that if b is false from the above, you'll break immediately.
    for (wres) |hint| {
        if (!b) break;
        switch (hint.index) {
            .incl => |_| {},
            .excl => |_| {},
            .never => |_| {
                if (std.mem.indexOfScalar(u8, &letter_set, hint.letter)) |_| {
                    b = b and false;
                } else {
                    b = b and true;
                }
            },
        }
    }

    return b;
}

// handled by arena allocator in caller
// TODO thin in place and resize?
fn thin_words(wmap: *const WordleMap, wres: HintSlice, allocator: std.mem.Allocator) !WordleMap {
    var out = WordleMap.init(allocator);
    var it = wmap.iterator();

    var check: bool = undefined;

    while (it.next()) |elem| {
        check = try check_word(elem.key_ptr.*, wres);
        if (check) {
            try out.put(elem.key_ptr.*, elem.value_ptr.*);
        }
    }

    return out;
}

// TESTS
const test_allocator = std.testing.allocator;

test "simple test" {
    const path = "./words_dictionary.json";
    const max_size = 100_000_000;
    var arena = AAllocator.init(test_allocator);
    defer arena.deinit();

    var allocator = arena.allocator();

    var wl = try read_json_wordle(path, 5, max_size, allocator);

    // only 5-letter words
    var x_null = wl.get("a");
    try std.testing.expect(x_null == null);

    const keys = wl.keys();

    try std.testing.expect(keys[0].len == 5);

    // score_scale of 0 zero gives score of # unique letters
    var freq_map = try letter_freq(keys, allocator, 0);
    try wl.setScores(freq_map);

    // TODO double-free? i think. gives segfault.
    //defer wl.deinit();

    var x = wl.get("acorn").?;
    try std.testing.expect(x == 5);
}

test "check_word and thin_words" {
    const word = [_][]const u8{ "acorn", "thorn", "aroma" };

    // Matches acorn, thorn
    const hint = [_]WordleHint{.{
        .letter = 'n',
        .index = IndexHint{
            .excl = 0,
        },
    }};
    // Matches acorn
    // n included at index 4, c included but not at 0
    const hint2 = [_]WordleHint{ .{
        .letter = 'n',
        .index = IndexHint{ .incl = 4 },
    }, .{ .letter = 'c', .index = IndexHint{ .excl = 0 } } };
    // Matches acorn, aroma
    // a included at least at 0
    const hint3 = [_]WordleHint{.{
        .letter = 'a',
        .index = IndexHint{ .incl = 0 },
    }};
    // Matches aroma
    // a included at least at 0, m included not at 1
    const hint4 = [_]WordleHint{ .{
        .letter = 'a',
        .index = IndexHint{ .incl = 0 },
    }, .{
        .letter = 'm',
        .index = IndexHint{ .excl = 1 },
    } };

    // Matches acorn but not aroma
    // a included at least at 0, but only there
    const hint5 = [_]WordleHint{ .{
        .letter = 'a',
        .index = IndexHint{ .incl = 0 },
    }, .{
        .letter = 'a',
        .index = IndexHint{ .never = {} },
    } };
    // Matches aroma but not acorn
    // a included at 4, and in some position != 1
    const hint6 = [_]WordleHint{ .{
        .letter = 'a',
        .index = IndexHint{ .incl = 4 },
    }, .{
        .letter = 'a',
        .index = IndexHint{ .excl = 1 },
    } };
    // Matches acorn but not aroma
    const hint7 = [_]WordleHint{.{
        .letter = 'a',
        .index = IndexHint{ .excl = 4 },
    }};
    // Matches aroma but not acorn
    const hint8 = [_]WordleHint{ .{
        .letter = 'a',
        .index = IndexHint{ .excl = 3 },
    }, .{
        .letter = 'a',
        .index = IndexHint{ .excl = 2 },
    } };

    // Matches none
    // n included but *not* at index 4
    const hint_bad = [_]WordleHint{.{
        .letter = 'n',
        .index = IndexHint{ .excl = 4 },
    }};
    // Matches none
    // n not included
    const hint_bad2 = [_]WordleHint{.{
        .letter = 'n',
        .index = IndexHint{ .never = {} },
    }};

    // CHECK WORD
    // true if it should be kept
    try std.testing.expect(try check_word(word[0], &hint));
    try std.testing.expect(try check_word(word[0], &hint2));
    try std.testing.expect(!(try check_word(word[1], &hint2)));

    try std.testing.expect(try check_word(word[0], &hint3));
    try std.testing.expect(!(try check_word(word[1], &hint3)));
    try std.testing.expect(try check_word(word[2], &hint3));

    try std.testing.expect(try check_word(word[2], &hint4));
    try std.testing.expect(!(try check_word(word[0], &hint4)));

    // TODO remove after debugging
    //try std.testing.expectError(std.mem.Allocator.Error.OutOfMemory, try check_word(word[0], &hint5));

    try std.testing.expect(try check_word(word[0], &hint5));
    try std.testing.expect(!(try check_word(word[2], &hint5)));

    try std.testing.expect(!(try check_word(word[0], &hint6)));
    try std.testing.expect(try check_word(word[2], &hint6));

    try std.testing.expect(try check_word(word[0], &hint7));
    try std.testing.expect(!(try check_word(word[2], &hint7)));

    try std.testing.expect(!(try check_word(word[0], &hint8)));
    try std.testing.expect(try check_word(word[2], &hint8));

    try std.testing.expect(!(try check_word(word[0], &hint_bad)));
    try std.testing.expect(!(try check_word(word[0], &hint_bad2)));

    // THIN
    var allocator = AAllocator.init(test_allocator);
    defer allocator.deinit();

    var wm = WordleMap.init(allocator.allocator());
    // not used when using arena alloc
    // wm.deinit();

    for (&word) |w| {
        try wm.put(w, 0);
    }

    // Filtering
    // TODO careful! in this case, memory is leaked. why?
    // but if you deinit, it crashes. double-free?
    //wm_thin = try thin_words(&wm, &hint_bad, test_allocator);
    //x = wm_thin.get("acorn");
    var wm_thinner = try thin_words(&wm, &hint_bad, allocator.allocator());
    var x = wm_thinner.get("acorn");
    try std.testing.expect(x == null);
    x = wm_thinner.get("thorn");
    try std.testing.expect(x == null);

    // No filtering should happen
    var wm_thin = try thin_words(&wm, &hint, allocator.allocator());
    // not used when using arena alloc
    //defer wm_thin.deinit();
    x = wm_thin.get("acorn");
    try std.testing.expect(x.? == 0);
    x = wm_thin.get("thorn");
    try std.testing.expect(x.? == 0);

    // Only acorn
    wm_thin = try thin_words(&wm, &hint2, allocator.allocator());
    // not used when using arena alloc
    //defer wm_thin.deinit();
    x = wm_thin.get("acorn");
    try std.testing.expect(x.? == 0);
    x = wm_thin.get("thorn");
    try std.testing.expect(x == null);

    // Try when modifying a single object
    // NOTE ON MEMORY
    // without the arena allocator (and with wm2.deinit), this creates some memory leaks. what's the source?
    var wm2 = WordleMap.init(allocator.allocator());
    for (&word) |w| {
        try wm2.put(w, 0);
    }

    wm2 = try thin_words(&wm2, &hint, allocator.allocator());
    var x2 = wm2.get("acorn");
    try std.testing.expect(x2.? == 0);
    x2 = wm2.get("thorn");
    try std.testing.expect(x2.? == 0);

    wm2 = try thin_words(&wm2, &hint_bad, allocator.allocator());
    x2 = wm2.get("acorn");
    try std.testing.expect(x2 == null);

    wm2 = try thin_words(&wm2, &hint_bad2, allocator.allocator());
    x2 = wm2.get("acorn");
    try std.testing.expect(x2 == null);
}

test "letter_freq" {
    var wl = [_][]const u8{ "aac", "aac", "bcb", "caa" };
    const scale = 3;
    var lf = try letter_freq(&wl, test_allocator, scale);
    defer lf.deinit();

    const a = lf.get('a').?;
    const aex: u64 = 1;
    const b = lf.get('b').?;
    const bex: u64 = 0;
    const c = lf.get('c').?;
    const cex: u64 = 1;

    try std.testing.expect(a == aex);
    try std.testing.expect(b == bex);
    try std.testing.expect(c == cex);

    // word scoring
    var score = try score_word(wl[0], lf);
    var score_ex: u64 = 4;
    try std.testing.expect(score == score_ex);

    score = try score_word(wl[2], lf);
    score_ex = 3;
    try std.testing.expect(score == score_ex);
}
